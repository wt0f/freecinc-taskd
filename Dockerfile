FROM alpine:3.16

ARG VERSION

LABEL MAINTAINER="Gerard Hickey <hickey@kinetic-compute.com>"
LABEL SOURCE="https://gitlab.com/wt0f/taskd"
LABEL TASKD_VERSION=$VERSION

RUN apk update && apk add taskd==$VERSION gnutls-utils && \
    mkdir -p /app
ADD startup.sh /app/

WORKDIR /app
USER taskd
VOLUME /data
EXPOSE 53589

ADD pki/ /app/pki/
ADD scripts/ /app/scripts/

ENV PATH=$PATH:/app/scripts
ENV TASKDDATA=/data
ENV SECRETS_DIR="${TASKDDATA}/pki"
CMD sh startup.sh
