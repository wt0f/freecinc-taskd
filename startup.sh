#! /bin/bash

# Check to see if we have a config file otherwise abort
if [[ ! -e "${TASKDDATA}/config" ]]; then
    echo "taskd does not seem to have a valid data directory in ${TASKDDATA}"
    exit 1
fi

# start up taskd
exec taskd server
