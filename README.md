taskd
=====

This is a fork of the original [freecinc-taskd](https://github.com/freecinc/freecinc-taskd).
The original repo would not build a useable Docker image so I forked the
repo and updated the `Dockerfile` and scripts to make a usable image.
Installation and administration of this image is documented here for any
other person to use.

Docker image
------------

The Docker image can be pulled with the following command:

    docker pull registry.gitlab.com/wt0f/taskd:VERSION

The `VERSION` tag should be the version that appears on the release pages
(https://gitlab.com/wt0f/taskd/-/releases/). At the time of this writing,
the latest release is `1.1.0`. Several of the following examples are
written using this version, but if you are running a newer version you
will need to update the command in the example with your version.

Initialize taskd data directory
-------------------------------

Before `taskd` can be started, the data directory needs to be initialized.
This is started by creating a directory where the `taskd` data files will
reside.

    % mkdir DIR/data
    % sudo chown -R 100:101 DIR/data
    % sudo chmod -R

It is convention to call the directory `data`, but it can be named anything
you wish to name it. Just be sure to use the same directory name on each
of the `docker` commands you execute.

You need to specify the physical location of the data directory and replace
`DIR` with it. This directory location will need to be specified on the
`docker` command line where you see `DIR/data`.

To initialize the data directory execute:

    % docker run -t -v DIR/data:/data registry.gitlab.com/wt0f/taskd:1.1.0 init-taskd
    Created /data/config

If you already have a `taskd` data directory that you are using, please skip
this step and proceed to generating or importing your certificates. It is
quite possible if you had a running server that all that will need to be
done is to update `config` with the location of the certificates. In which
case you can skip to [Modify server configuration](#modify-server-configuration),
but please read the following about insuring your `data` directory has the
correct permissions.

### Data directory permissions

As noted in [this issue](https://gitlab.com/wt0f/taskd/-/issues/7) the `data`
directory needs to have specific ownership in order for the container to
operate correctly. It can not be stated strongly enough that the `taskd`
container **should not** be running as the `root` user. This can lead to
lessening system security--actually no container should execute as `root`
as there are only a select few images that have been hardened and even then
**only** when absolutely necessary!

The `data` directory and all the files underneath this directory should be
owned by the UID `100` and GID `101`. It is highly recommended that the
permissions be set read/write only for the file owner (i.e. UID `100`).
For example:

    % ls -lan
    total 16
    drwxr-xr-x  3   0   0 4096 Jan 25 23:44 .
    drwxr-xr-x 23   0   0 4096 Jan 25 23:36 ..
    drwx------  4 100 101 4096 Jan 25 23:37 data


For those that are not as familiar with the Linux command line, the following
commands will secure your `data` directory.

    % sudo chown -R 100:101 DIR/data
    % sudo chmod -R u+rw,g-rwx,o-rwx DIR/data

`sudo` is used here because only the superuser is able to change the owner
of the directory and set the permissions of the directory because the
directory is now owned by another user.

Generate server certificates
----------------------------

To generate a self-signed certificate for the server, one will need to
execute the following command:

    % docker run -t -v DIR/data:/data registry.gitlab.com/wt0f/taskd:1.1.0 gen-certs CN=`hostname -f`
    ** Note: You may use '--sec-param High' instead of '--bits 4096'
    Generating a 4096 bit RSA private key...
    Generating a self signed certificate...
    X.509 Certificate Information:
    	Version: 3
    	Serial Number (hex): 447e1127f8dac8c20b052ddd9f6812f39ffd2456
    .... (certificate generation output continues for some time)

    Signing certificate...

Valid `ATTRS` are

- CN
- ORGANIZATION
- LOCALITY
- STATE
- COUNTRY
- BITS (default: 4096)
- EXPIRATION_DAYS (default: 365)

Each of these attributes needs to be formatted as ATTR=VALUE. The only
required attribute that needs to be set is `CN`. This should be set to the
FQDN hostname of the machine that will be running `taskd`. In the example
above the `CN` is set to the FQDN of the current host.

Once this command is executed the certificates will be generated and the
configuration will be updated with the proper certificate settings.

Importing server certificates
-----------------------------

If you have certificates that need to be imported, then there are two ways
to achieve this. The first is to create a `pki` directory and mount it
into the running container. This would also require that `SECRETS_DIR` be
set to the mounted location.

The second approach is to create a `pki` directory in the `taskd` data
directory. The `SECRETS_DIR` will automatically get set when the container
is started and the `pki` directory is found in the `taskd` data directory.

In both cases the certificate parameters will still need to be added to the
`taskd` `config` file. This can be done manually by editing the file
directly or by using the using the procedure below for editing the
configuration.

Starting the server
-------------------

Once the `taskd` data directory and certificates have been initialized or
imported the server is ready to be started. The server can be started with:

    % docker run --name taskd --restart always -p 53589:53589 -v DIR/data:/data registry.gitlab.com/wt0f/taskd:1.1.0

This assumes that you are running the server on the standard port of 53589.
If you are using another port, please update the `-p` parameter to 53589:PORT.

To aid in running the server under a process supervisor, there are files
located in the `support` directory of the repo (http://gitlab.com/wt0f/taskd/)
that can be used to setup `taskd` to run under `systemd`, `supervisord` or other
supervisor. If you have a configuration file for another supervisor please
feel free to create an issue in the repo with the file (or create the MR
yourself) to add support for the supervisor.

Add or remove user/organization
-------------------------------

Now that the `taskd` server is running, organizations and users can be
created. At least one organization needs to be added before a user can
be created. To add an organization execute:

    % docker exec -t taskd add org ORG_NAME
    Created organization 'ORG_NAME'

Afterwards a user can be added with:

    % docker exec -t taskd add user ORG_NAME USER_NAME
    New user key: 65c016b1-f330-4544-821e-bb01aa9206f8
    Created user 'USER_NAME' for organization 'ORG_NAME'

This will not create the user certificates. It is actually best to create
a user with the `create-user` command which will create the user and
generate the user's certificates.

    % docker exec -t taskd create-user ORG_NAME USER_NAME
    New user key: b66fcd5c-dd1e-4d6f-9833-088c02f4d7e6
    Created user 'USER_NAME' for organization 'ORG_NAME'
    === USER_NAME key file follows ===
    (Contents of USER_NAME.key.pem)
    === USER_NAME certificate file follows ===
    (Contents of USER_NAME.cert.pem)
    === CA certificate file follows ===
    (Contents of ca.cert.pem)

The above output is quite large and most of it has been trimmed out. With
the output, a user can construct the configuration and necessary files for
communicating with `taskd`. Please refer to https://taskwarrior.org/docs/taskserver/taskwarrior/
for the proper configuration of Taskwarrior to communicate with `taskd`.

To remove an entity, one can execute `remove` instead of `add`. For example:

    % docker exec -t taskd remove org ORG_NAME
    Removed organization 'ORG_NAME'

Modify server configuration
---------------------------

The `taskd` server can be configured with the `config` command. This command
will only update the configuration and it up to the user to restart the
server to activate the new configuration. Here is a simple example:

    % docker exec -t taskd config pid.file /tmp/pid
    Config file /data/config modified.

A full list of configuration variables can be found at
https://manpages.ubuntu.com/manpages/focal/man5/taskdrc.5.html

Server diagnostics
------------------

Should it be necessary for troubleshooting purposes, one can get the
`taskd` diagnostics with the following command:

    % docker exec -t taskd diagnostics

    taskd 1.1.0
        Platform: Linux
        Hostname: 0997491f38ed

    Compiler
         Version: 11.2.1 20220219
            Caps: +stdc +stdc_hosted +200809 +LP64 +c8 +i32 +l64 +vp64 +time_t64
      Compliance: C++11

    Build Features
           Built: Mar 17 2020 00:13:15
           CMake: 3.23.1
         libuuid: libuuid + uuid_unparse_lower
       libgnutls: 3.7.4
      Build type: None

    Configuration
       TASKDDATA: /data
            root: /data (readable)
          config: /data/config (readable)
              CA: /data/pki/ca.cert.pem (readable)
     Certificate: /data/pki/server.cert.pem (readable)
             Key: /data/pki/server.key.pem (readable)
             CRL: /data/pki/server.crl.pem (readable)
             Log: /dev/stdout (found)
        PID File: /run/taskd.pid (missing)
          Server: 0.0.0.0:53589
     Max Request: 0 bytes
         Ciphers:
           Trust: strict
